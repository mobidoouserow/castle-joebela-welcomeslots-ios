#import <UIKit/UIKit.h>

@interface UIColor (Poloblue)

+ (UIColor*) poloBlue;

@end
