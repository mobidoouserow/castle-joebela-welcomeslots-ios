#import "UIColor+Poloblue.h"

@implementation UIColor (Poloblue)

+ (UIColor*) poloBlue {

	/**
	 name: Polo Blue
	 red: 0.5568627451
	 green: 0.7137254902
	 blue: 0.8156862745
	 alpha: 1.0000000000
	 hex: #8EB6D0
	 **/

	return [UIColor colorWithRed:0.5568627451 green:0.7137254902 blue:0.8156862745 alpha:1.0000000000];
}


@end
