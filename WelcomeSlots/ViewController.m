//
//  ViewController.m
//  WelcomeSlots
//
//  Created by alexandr on 8/16/17.
//  Copyright © 2017 joebella. All rights reserved.
//

#import "ViewController.h"

#import "UIColor+Poloblue.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIWebView *gameView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor poloBlue];
    
    NSURL *url = [NSURL fileURLWithPath:[NSBundle.mainBundle pathForResource:@"index"
                                                                      ofType:@"html"
                                                                 inDirectory:@"WelcomeSlotsGame"]];
    
    
    [_gameView loadRequest:[NSURLRequest requestWithURL:url]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
